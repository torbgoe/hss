import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial

l = 2
t = 3

x = np.arange(11)

P= np.power(l*t, x)/factorial(x, exact=False)*np.exp(-l*t)
plt.stem(x,P)
plt.show()
print(P)

