import numpy as np
from matplotlib import pyplot as plt
from scipy import stats, integrate
import pandas as pd
from math import pi, sqrt

# Normal distribution
def f(x):
    mean = 26.13
    std = 5.06
    return 1 / (sqrt(2*pi)*std) * np.exp(-np.power(x-mean,2) / (2*std**2))


data = np.array([22,23,19,28,27,26,25,24,33,30,20,28,26,24,43,27,18,15,25,32,25,32,20,29,23,36,19,25,27,30,17,20,24,27,24,35,27,23,29,23,30,31,35,26,28,28,22,30,21,29,30,18,27,23,25,27,29,28,28,23])
x = np.linspace(data.min(), data.max(), 1000)
#data = data[0:1]
# Calculate 
print("Mean: {0} \nMedian: {1} \nMode: {2} Occurences: {3} \nMin: {4} \nMax: {5} \nMAD: {6} \nStd: {7}"
    .format(
        np.mean(data),
        np.median(data),
        stats.mode(data)[0],
        stats.mode(data)[1],
        np.min(data),
        np.max(data),
        pd.Series(data).mad(),
        np.std(data)
    )
)
#print(np.sort(data))


# Plot with high number of bins
fig, ax1 = plt.subplots()
num_bins = data.max() - data.min()
n, bins, patches = ax1.hist(data, num_bins, facecolor='blue', alpha=0.6, edgecolor='black')
ax2 = ax1.twinx()
ax2.plot(x,f(x), color="red")
# Plot with smaller number of bins
fig, ax1 = plt.subplots()
num_bins = 7
n, bins, patches = ax1.hist(data, num_bins, facecolor='blue', alpha=0.6, edgecolor='black')
ax2 = ax1.twinx()
ax2.plot(x,f(x), color="red")
# Plot cumulative normal distribution
fig, ax1 = plt.subplots()
cP = np.zeros(1000)
i=0
for x_i in x:
    cP[i] = integrate.quad(f,0,x_i)[0]
    i += 1
cP_25 = integrate.quad(f,0,25)
print(cP_25)
ax1.plot(x,f(x), color="red")
ax1.axvline(x=25,color='red')
ax2 = ax1.twinx()
ax2.plot(x,cP,color='blue')
plt.grid(1)
#plt.show()



