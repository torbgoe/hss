import numpy as np
from scipy.special import comb
import matplotlib.pyplot as plt


p = 1/6
n = 6
r = np.arange(7) # [0-6]

P = comb(n, r)*np.power(p, r)*np.power(1-p, n-r)
plt.stem(r,P)
plt.show()
print(P)

