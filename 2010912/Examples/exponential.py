import numpy as np
import matplotlib.pyplot as plt 

def f(t):
    return l*np.exp(-l*t)

def F(t):
    return 1-np.exp(-l*t)

n = 1000
l = 2
t = np.linspace(0,2,n)

P = f(t)
cP = F(t)
plt.plot(t, P)
plt.fill_between(t, P, 0, where=t<=0.25)
plt.plot(t,cP,color="red")
plt.xlim(0,2)
plt.grid(1)
plt.show()

print(1-np.exp(-2*0.25))