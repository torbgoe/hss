import numpy as np
import matplotlib.pylab as plt
from scipy import integrate, stats
from math import factorial, exp, pi, sqrt


''' Problem 6.1 '''
print("\n\nProblem 6.1\n")
data = np.array([49.3,50.1,49.0,49.2,49.3,50.5,49.9,49.2,49.7,50.2])
data_min = data.min()
n_bins = 8 #number of bins
w_bins = 0.2 #width of bins
bins = np.linspace(data_min, data_min + n_bins * w_bins, n_bins + 1)
hist, bins = np.histogram(data, bins=bins) # hist holds the distribution, if you'd want to use it further
plt.hist(data, bins=bins, facecolor='blue', alpha=0.6, edgecolor='black')
plt.xlabel("x [cm]")
plt.ylabel("# of occurences")
#plt.show()
print("{} occurences in bins {}".format(hist, bins))


''' Problem 6.5 '''
print("\n\nProblem 6.5\n")
print("Mean: {0} \nMedian: {1} \nMode: {2} Occurences: {3} \nMin: {4} \nMax: {5} \nStd: {6}"
    .format(
        np.mean(data),
        np.median(data),
        stats.mode(data)[0],
        stats.mode(data)[1],
        np.min(data),
        np.max(data),
        np.std(data)
    )
)
print("(scipy.stats.mode only returns the lowest mode, 49.3 should also be a mode)")

''' Problem 6.14 '''
print("\n\nProblem 6.14\n This is a binomial distribution.\n")
P = 0.02*0.02
print("Probability both components are defective: {}".format(P))

''' Problem 6.24 '''
print("\n\nProblem 6.24\n This is a binomial distribution.\n")
def ncr(n, r):
    return factorial(n) / (factorial(r) * factorial(n-r))
def binom(p, n, r):
    return ncr(n,r) * p**r * (1-p)**(n-r)
p = 0.95
n = 4
# a)
print("a)")
P = p**4 # Simplified, whole equation would be ncr(4,4) * p**4 * (1-p)**(4-4)
print("Probability of all selected parts being satisfactory: {}".format(P))
# b)
print("b)")
P = binom(p,n,2) + binom(p,n,3) + binom(p,n,4)
print("Probability of at leats 2 selected parts being satisfactory: {}".format(P))

''' Problem 6.27 '''
print("\n\nProblem 6.27\n This is a binomial distribution.\n")
n=6
# a)
print("a)")
P = binom(p,n,6)
print("Probability of all selected parts being satisfactory: {}".format(P))
# b)
print("b)")
P = binom(p,n,2) + binom(p,n,3) + binom(p,n,4) + binom(p,n,5) + binom(p,n,6)
print("Probability of at leats 2 selected parts being satisfactory: {}".format(P))

''' Problem 6.36 '''
print("\n\nProblem 6.36\n Assume a poisson distribution\n")
def poisson(l, t, x):
    return (l*t)**0 * exp(-l*t)
l = 1/4 # lambda: 1 crash every 4 days, 0.25 crashes/day
t = 5 # 5 days
x = 0 # No crashes
P = poisson(l, t, x)
print("Probability of {} failures in {} days: {}".format(x, t, P))

''' Problem 6.44 '''
print("\n\nProblem 6.44\n Assume a normal distribution\n")
print(
    "Find the standard deviation using table 6.3.\n"
    "~0.25 on one side of the expected value gives z = 0.67 (between 0.67 and 0.68)\n"
    "Given z = 0.67 = (110.5 - 110) / std --> std = (110.5 - 110) / 0.67"
)
std = (110.5 - 110) / 0.67
print("std = {:.2f}\n".format(std))

z = (111 - 110) / 0.75
print(
    "z-value for 1 V or less error: (111 - 110) / 0.75:\n"
    "\tz = {:.2f}\n".format(z)
)

print(
    "Find probability error 1 V or LESS from table 6.3:\n"
    "\tP_less_1V = 2*0.4082 = 0.8164 (table is one-sided)\n"
    "Probability of error GREATER than 1 V:\n"
    "\tP_more_1V = 1 - P_less_1V = 1 - 0.8164 = 0.1836"
)

''' Problem 6.44 '''
print("\n\nProblem 6.44\n Assume a normal distribution\n")
errors = [1,2,5,10]
mean = 775
std = 1
n = 40 # number of measurements
def normal(x):
    mean = 775 # mean and std hardcoded for numerical integration
    std = 1
    return 1 / (sqrt(2*pi)*std) * np.exp(-np.power(x-mean,2) / (2*std**2))

for e in errors:
    P = integrate.quad(normal,mean-e,mean+e)[0]
    S = n*P #number of successes
    print("Probability of measurement within {} cm: {:.2f}\nNumber of successes (measurement within error): {:.0f}".format(e, P, S))

# Show figures after all everything else is done
plt.show()